//
//  ViewController.swift
//  testproject
//
//  Created by Anders on 2015-04-20.
//  Copyright (c) 2015 Anders Widén. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var displayData: UITextView!
    var hello = Array<Float>()
    
    @IBAction func downloadBtn(sender: AnyObject) {
        
        let urlPath: String = "http://data.goteborg.se/AirQualityService/v1.0/LatestMeasurement/9307444e-8e16-4859-99d7-5a69d0d777b0?format=Json"
        var url: NSURL = NSURL(string: urlPath)!
        var request1: NSURLRequest = NSURLRequest(URL: url)
        let queue:NSOperationQueue = NSOperationQueue()
        NSURLConnection.sendAsynchronousRequest(request1, queue: queue, completionHandler:{ (response: NSURLResponse!, data: NSData!, error: NSError!) -> Void in
            var err: NSError
            var jsonResult: NSDictionary = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: nil) as! NSDictionary
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                println(jsonResult.valueForKey("Weather")?.valueForKey("Humidity")?.valueForKey("Value"))
                var hej = String(stringInterpolationSegment: jsonResult.valueForKey("Weather")?.valueForKey("Humidity")?.valueForKey("Value"))
                self.displayData!.text = hej
            })
        })
        
    }
    
    func done() {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

